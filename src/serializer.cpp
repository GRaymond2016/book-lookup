#include "serializer.hpp"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

const static std::string filename = "database.txt";

// This serialization as far as I'm aware does a TLV write of all internal elements.
// Should I handroll a solution it would likely be the same type of implementation, though maybe assuming the type is always std::string.

// O(n) on the collection iteration.
void serializer_save_collection(std::ostream &stream, collection& coll)
{
    try
    {
        boost::archive::text_oarchive archive(stream);
        archive << coll.get_map();
    }
    catch (const boost::archive::archive_exception& ex)
    {
        std::cout << ex.what() << std::endl;
    }
}

// O(n) on the collection iteration
void serializer_load_collection(std::istream &stream, collection& coll)
{
    try
    {
        boost::archive::text_iarchive archive(stream);
        archive >> coll.get_map();
    }
    catch (const boost::archive::archive_exception& ex)
    {
        std::cout << ex.what() << std::endl;
    }
}