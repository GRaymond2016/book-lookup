#ifndef SERIALIZER_H
#define SERIALIZER_H

#include <iostream>
#include "collection.hpp"

void serializer_save_collection(std::ostream &stream, collection& coll);
void serializer_load_collection(std::istream &stream, collection& coll);

#endif