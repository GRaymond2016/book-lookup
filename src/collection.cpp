#include "collection.hpp"
#include <iostream>

//Constant time, worst case O(n)
book_description collection::lookup(const std::string &key)
{
    auto iter = map.get<title_tag>().find(key);
    if (iter == map.get<title_tag>().end())
        return book_description();
    return *iter;
}
//Constant time, worst case O(n)
book_description collection::lookup_by_author(const std::string &key)
{
    auto iter = map.get<author_tag>().find(key);
    if (iter == map.get<author_tag>().end())
        return book_description();
    return *iter;
}
//Constant time, worst case O(n)
void collection::add_entry(book_description desc)
{
    map.insert(desc);
}
//Constant time, worst case O(n)
int collection::remove_all_by_author(const std::string &authr)
{
    return map.get<author_tag>().erase(authr);
}
//Constant time, worst case O(n)
int collection::remove_entry(const std::string& title)
{
    return map.erase(title);
}
book_map_t& collection::get_map()
{
    return map;
}