#include <stdio.h>
#include <iostream>
#include <string>
#include <sstream>

#include <cassert>
#include <fstream>

#include "serializer.hpp"
#include "collection.hpp"

static const std::string g_persistence_file = "data.store";

//Test some basic usage of the collection, I don't have to go into too much depth given boost tests this for me.
void collection_test()
{
    collection coll;
    coll.add_entry(book_description("Title", "Author"));

    book_description &res = coll.lookup("Title");
    assert(strcmp(res.title.c_str(), "Title") == 0);

    res = coll.lookup_by_author("Author");
    assert(strcmp(res.author.c_str(), "Author") == 0);

    res = coll.lookup_by_author("Title");
    assert(res.valid == false);

    coll.add_entry(book_description("Title", "Author"));
    coll.add_entry(book_description("Title", "Author"));
    coll.add_entry(book_description("Title1", "Author"));
    coll.add_entry(book_description("Title2", "Author"));

    res = coll.lookup("Title");
    assert(strcmp(res.title.c_str(), "Title") == 0);

    coll.remove_entry("Title");
    res = coll.lookup("Title");
    assert(res.valid == false);
    res = coll.lookup("Title1");
    assert(res.valid == true);
    coll.remove_all_by_author("Author");
    res = coll.lookup("Title1");
    assert(res.valid == false);
}

//Basic serialization test, does it serialize more than 1 entry into an in-memory stream.
void serialization_test()
{
    std::stringstream ss;
    collection coll, coll2;
    coll.add_entry(book_description("Title", "Author"));
    coll.add_entry(book_description("Title1", "Author"));
    coll.add_entry(book_description("2Title", "Author1"));
    coll.add_entry(book_description("Title4", "Author1"));
    serializer_save_collection(ss, coll);
    serializer_load_collection(ss, coll2);
    book_description &res = coll2.lookup("Title");
    assert(res.valid == true);
    res = coll2.lookup("Title1");
    assert(res.valid == true);
    res = coll2.lookup("2Title");
    assert(res.valid == true);
    res = coll2.lookup("Title4");
    assert(res.valid == true);
    res = coll2.lookup_by_author("Author1");
    assert(res.valid == true);
}

void unit_tests()
{  
    collection_test();
    serialization_test();
}

void print_record(const book_description &res)
{
    std::cout << "----RECORD-----" << std::endl;
    std::cout << "Title: " << res.title << std::endl;
    std::cout << "Author: " << res.author << std::endl;
    std::cout << "---------------" << std::endl;
}

//I started writing a MUD, but this is all that came out..
void input_loop(collection &coll)
{
    std::cout << "Type 'quit' to save and quit application." << std::endl;
    while (true)
    {
        std::string input;
        int type;
        std::cout << "What do you want to do?" << std::endl <<
                     "  1) Title Lookup" << std::endl <<
                     "  2) Author Lookup" << std::endl <<
                     "  3) Add Entry" << std::endl <<
                     "  4) Remove Entry by Title" << std::endl <<
                     "  5) Remove Entry by Author" << std::endl;
        std::getline(std::cin, input);
        if (input.compare("quit") == 0)
            return;
        std::stringstream ss(input);
        if (!(ss >> type))
        {
            std::cout << "Invalid entry." << std::endl;
            continue;
        }
        book_description res;
        switch (type)
        {
        case 1:
        {
            std::cout << "What is the title?" << std::endl;
            std::getline(std::cin, input);
            if (input.compare("quit") == 0)
                return;
            res = coll.lookup(input);
            if (res.valid)
                print_record(res);
            else
                std::cout << "Could not find any records." << std::endl;
            break;
        }
        case 2:
        {
            std::cout << "Who is the author?" << std::endl;
            std::getline(std::cin, input);
            if (input.compare("quit") == 0)
                return;
            res = coll.lookup_by_author(input);
            if (res.valid)
                print_record(res);
            else
                std::cout << "Could not find any records." << std::endl;
            break;
        }
        case 3:
        {
            std::string author, title;
            std::cout << "Who is the author?" << std::endl;
            std::getline(std::cin, author);
            std::cout << "What is the title?" << std::endl;
            std::getline(std::cin, title);

            coll.add_entry(book_description(title, author));
            std::cout << "Added Entry -- Title <" << title << "> Author <" << author << ">" << std::endl;
            break;
        }
        case 4:
        {
            std::string title;
            std::cout << "What is the title?" << std::endl;
            std::getline(std::cin, title);
            coll.remove_entry(title);
            break;
        }
        case 5:
        {
            std::string title;
            std::cout << "Who is the author?" << std::endl;
            std::getline(std::cin, title);
            coll.remove_all_by_author(title);
            break;
        }
        default:
            continue;
        }
    }
}

int main()
{
    //If this were a bigger example I would have this as a seperate project.
    unit_tests();

    collection coll;
    //Load the previous state of the program given in the specified file, populate the collection with this data
    serializer_load_collection(std::fstream(g_persistence_file, std::fstream::in), coll);
    //Main loop where the user interacts with the program
    input_loop(coll);
    //Save the state of the program to disk
    serializer_save_collection(std::fstream(g_persistence_file, std::fstream::out | std::fstream::trunc), coll);
    return 0;
}

