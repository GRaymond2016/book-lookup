#ifndef COLLECTION_H
#define COLLECTION_H

#include <string>
#include <stdlib.h>

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/hashed_index.hpp>

//Structures are used as tags for the different indicies in a boost multi index
struct title_tag {};
struct author_tag {};

//This describes our data structure that will hold each book, it has to define a serialization function for boost serialization
//The valid flag is placed to show if it was instanstiated with data or not. Probably not my best design choice.
struct book_description
{
    friend class boost::serialization::access;

    std::string title;
    std::string author;
    bool valid;

    book_description(const std::string& tit, const std::string& auth) : title(tit), author(auth), valid(1)
    {}
    book_description() : valid(0)
    {}

    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
        ar & title;
        ar & author;
        ar & valid;
    }
};

// We assume that the title is unique, and that no two books have the same title.
typedef boost::multi_index_container <
    book_description,
    boost::multi_index::indexed_by<
    boost::multi_index::hashed_unique< boost::multi_index::tag<title_tag>, BOOST_MULTI_INDEX_MEMBER(book_description, std::string, book_description::title)>,
    boost::multi_index::hashed_non_unique< boost::multi_index::tag<author_tag>, BOOST_MULTI_INDEX_MEMBER(book_description, std::string, book_description::author)>
    > > book_map_t;

class collection
{
public:
    book_description lookup(const std::string &title);
    book_description lookup_by_author(const std::string &author);

    void add_entry(book_description desc);
    int remove_entry(const std::string &title);
    int remove_all_by_author(const std::string &authr);

    book_map_t& get_map();

private:
    book_map_t map;
};

#endif